#!/usr/bin/env node
import yargs = require('yargs/yargs');
import { FreedcampApi } from './FreedcampApi'
import { ITaskFilters, ITask } from './freedcamp_api/use_cases/GetTasks'
import { IAddTask } from './freedcamp_api/use_cases/AddTask'

//type Difficulty = 'add' | 'ls' | 'ps';
//const difficulties: ReadonlyArray<Difficulty> = ['add', 'ls', 'ps'];

//const processCommand = (fc: FreedcampApi, argv): void => {
  //const command = argv.command
  //if (command == 'ls') {
  //} else if (command == 'add') {
    //fc.addTask({project_id: 1392862, title: argv.title, description: argv.description, task_group_name: argv.list_name} as IAddTask).then((result: string) => {
      //console.log(result)
    //})
  //}
//}

//yargs(process.argv.slice(2))
  //.command('fc <command>', 'access the freedcamp api', (yargs) => {
    //yargs
      //.positional('command', {
        //describe: 'command to run',
        //default: 'ls'
      //})
  //}, (argv) => {
    //if (argv.verbose) console.log(argv.command)
    //processCommand(fc, argv)
  //})
  //.options({
    //command: {choices: difficulties},
    //api_key: { alias: 'a', type: 'string', demandOption: true, description: 'Run with verbose logging' },
    //title: { alias: 't', type: 'string', demandOption: true, description: 'Title of the task' },
    //description: { alias: 'm', type: 'string', description: 'Message / description of the task' },
    //list_name: { alias: 'l', type: 'string', demandOption: true, description: 'List name to add the task to' },
    //verbose: { alias: 'v', type: 'boolean', default: false, description: 'Run with verbose logging' }
  //}).argv;
//console.log(argv)


const listTasks = {
  command: 'ls',
  desc: 'List the tasks',
  builder: (yargs) => yargs.options( {
    project: { alias: 'p', type: 'string', demandOption: true, description: 'ID of the project' },
  }),
  handler: (argv) => {
    fc.getTasks({project_id: argv.project} as ITaskFilters).then((result: ITask[]) => {
      console.log(result)
    })
    argv._handled = true
  }
}

const addTask = {
  command: 'add',
  desc: 'List the tasks',
  builder: (yargs) => yargs.options( {
    title: { alias: 't', type: 'string', demandOption: true, description: 'Title of the task' },
    project: { alias: 'p', type: 'string', demandOption: true, description: 'ID of the project' },
    description: { alias: 'm', type: 'string', description: 'Message / description of the task' },
    list_id: { alias: 'l', type: 'string', demandOption: true, description: 'List name to add the task to' },
    assigned_to_id: { alias: 'u', type: 'number', demandOption: true, description: 'The assignee of the ticket', default: '0' },
  }),
  handler: (argv) => {
    const fc = new FreedcampApi(argv.api_key as string)
    fc.addTask({project_id: argv.project, title: argv.title, description: argv.description, task_group_id: argv.list_id, assigned_to_id: argv.assigned_to_id} as IAddTask).then((result: string) => {
      console.log(result)
    })
    argv._handled = true
  }
}

const tasksApi = {
  command: 'tasks',
  desc: 'Access the tasks api',
  builder: (yargs) => yargs
    .command(listTasks)
    .command(addTask)
    .option('tasksApi-option', {
      desc: 'command 1 option 1',
      type: 'string',
      global: true // <-- so it applies to listTasks too
    }),
  handler: (argv) => {
    if (!argv._handled) console.log('tasksApi handler:', argv)
    argv._handled = true
  }
}



const importTogglData = {
  command: 'import',
  desc: 'Access the time api',
  builder: (yargs) => yargs
}

const importData = {
  command: 'import',
  desc: 'Access the time api',
  builder: (yargs) => yargs
  .command(importTogglData)
}

const addTimeRecord = {
  command: 'add',
  desc: 'List the tasks',
  builder: (yargs) => yargs.options( {
  }),
  handler: (argv) => {
    const fc = new FreedcampApi(argv.api_key as string)
    fc.addTimeRecord(undefined).then((response) => {
      console.log(JSON.stringify(response.data));
    })
    argv._handled = true
  }
}

const timeApi = {
  command: 'time',
  desc: 'Access the time api',
  builder: (yargs) => yargs
    .command(importData)
    .command(addTimeRecord)
    .option('timeApi-option', {
      desc: 'command 2 option 1',
      type: 'string'
    }),
  handler: (argv) => {
    if (!argv._handled) console.log('timeApi handler:', argv)
    argv._handled = true
  }
}

const argv = yargs(process.argv.slice(2))
  .command(tasksApi)
  .command(timeApi)
  .options({
    api_key: { alias: 'a', type: 'string', demandOption: true, description: 'Run with verbose logging' },
    verbose: { alias: 'v', type: 'boolean', default: false, description: 'Run with verbose logging' }
  })
  .help()
  .wrap(null)
  .argv

if (!argv._handled) console.log('outer:', argv)
