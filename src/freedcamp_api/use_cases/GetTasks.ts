import axios, { AxiosRequestConfig } from 'axios';
import querystring from 'querystring';

export interface ITask {
  id: string,
  h_parent_id: string,
  h_top_id: string,
  h_level: number,
  f_adv_data: boolean,
  assigned_to_id: string,
  created_by_id: string,
  created_ts: number,
  task_group_id: string,
  project_id: string,
  priority: number,
  title: string,
  description: string,
  status: number,
  order: number,
  ms_order: number,
  comments_count: number,
  files_count: number,
  completed_ts: number,
  start_ts: string,
  due_ts: string,
  task_group_name: string,
  f_archived_list: boolean,
  priority_title: string,
  status_title: string,
  assigned_to_fullname: string,
  r_rule: string,
  f_r_icon: boolean,
  f_matched: boolean,
  f_visible: boolean,
  children: Array<unknown>,
  cf_tpl_id: string,
  url: string,
  ms_id: string,
  app_id: string
}

export interface ITaskFilters {
  status?: 0 | 1 | 2 ;
  project_id?: number;
  assigned_to_id?: number | string;
  created_by_id?: string;
  f_with_archived?: 1 | 0;
  lists_status?: 'active' | 'archived' | 'all'
}


export default function execute(apiKey: string, base_url: string): unknown {
  return (options: ITaskFilters): Promise<ITask[]> => {
    const url = base_url + '/tasks'

    const queryElems: Record<string, string | number | boolean | null | undefined> = {}
    queryElems.api_key = apiKey
    if(options.status)          queryElems.status          = options.status
    if(options.project_id)      queryElems.project_id      = options.project_id
    if(options.assigned_to_id)  queryElems.assigned_to_id  = options.assigned_to_id
    if(options.created_by_id)   queryElems.created_by_id   = options.created_by_id
    if(options.f_with_archived) queryElems.f_with_archived = options.f_with_archived
    if(options.lists_status)    queryElems.lists_status    = options.lists_status

    const query = querystring.stringify(queryElems);
    console.log(`${url}?${query}`)
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${url}?${query}`,
    };

    return new Promise((resolve, reject): void => {
      axios(config).then((result) => {
        const data = result.data.data.tasks
        resolve(data)
      }).catch((err) => {
        reject(err)
      })
    })
  }
}
