import { ITimeRecordOptions } from './freedcamp_api/use_cases/AddTimeRecord';
import { IFileProcessor } from './IFileProcessor';

export interface IClockifyEntry {
  ToBeDefined: string;
}

export class ClockifyFileProcessor implements IFileProcessor {
  public process = (_fileName: string, _myId: string, _mapping: Record<string, string>): Promise<ITimeRecordOptions[]> => {
    throw('Not yet implemented');
  }
}
