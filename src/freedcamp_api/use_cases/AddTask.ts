import axios, { AxiosRequestConfig } from 'axios';
import formData from 'form-data';

export interface IAddTask {
  title: string,
  description?: string,
  project_id: string | number,
  task_group_id?: number,
  task_group_name?: string,
  priority?: number,
  assigned_to_id?: number,
  start_date?: string,
  r_rule?: string,
  due_date?: string,
  attached_ids?: Array<number>,
  intern_parent_id?: string
}

export default function execute(apiKey: string, baseUrl: string): unknown {
  return (options: IAddTask): Promise<string> => {
    const url = baseUrl + '/tasks'
    const data = new formData();
    data.append('data', JSON.stringify(options));
    const config: AxiosRequestConfig = {
      data,
      method: 'POST',
      url: `${url}?api_key=${apiKey}`,
      headers: data.getHeaders()
    };
    console.log(options)

    return new Promise((resolve, reject): void => {
      axios(config).then((result) => {
        resolve(JSON.stringify(result.data))
      }).catch((err) => {
        reject(err)
      })
    })
  }
}
