import csv from 'csv-parser';
import fs from 'fs';

interface IMappingCsvEntry {
  freedcamp_project: string;
  id: string;
  toggl_reference: string;
}

export class MappingProcessor {
  public processMappingFile = (fileName: string): Promise<Record<string, string>> => {
    const records: Record< string, string > = {};
    return new Promise((resolve, reject) => {
      fs.createReadStream(fileName)
        .pipe(csv())
        .on('error', reject)
        .on('data', (row: IMappingCsvEntry) => {
          records[row.toggl_reference] = row.id
        })
        .on('end', () => {
          resolve(records)
        })
    })
  }
}

