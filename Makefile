.DEFAULT_GOAL := all

all: install clean watch

convert:
	echo 'Converting weird data file to normal csv'
	sed -ie '1 s/^.//' data.csv

install:
	yarn install

watch:
	echo 'Watching for changes'
	npx tsc -w

build:
	npx tsc

clean:
	rm -rf dist
