import getTasksImpl from './freedcamp_api/use_cases/GetTasks';
import addTaskImpl from './freedcamp_api/use_cases/AddTask';
import addTimeRecordImpl from './freedcamp_api/use_cases/AddTimeRecord';

export class FreedcampApi {
  private apiKey;
  private BASE_URL = 'https://freedcamp.com/api/v1';
  public getTasks;
  public addTask;
  public addTimeRecord;

  constructor(apiKey: string) {
    this.apiKey = apiKey;
    this.getTasks      = getTasksImpl(this.apiKey, this.BASE_URL)
    this.addTask       = addTaskImpl(this.apiKey, this.BASE_URL)
    this.addTimeRecord = addTimeRecordImpl(this.apiKey, this.BASE_URL)
  }
}
//string	Check constants wiki for possible values. For example, to get only not started and started tasks, you should use:  "/tasks?status[]=0&status[]=2"
