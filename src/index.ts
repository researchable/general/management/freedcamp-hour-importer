import { FreedcampApi} from './FreedcampApi';
import { ITimeRecordOptions } from './freedcamp_api/use_cases/AddTimeRecord';
import { ToggleFileProcessor } from './TogglFileProcessor';
import { MappingProcessor } from './Mapping';
import { IFileProcessor } from './IFileProcessor';

const apiKey = '';
const myId = '';
const actuallyRun = false;

const processor: IFileProcessor = new ToggleFileProcessor();
const freedcampApi: FreedcampApi = new FreedcampApi(apiKey);
const mappingProcessor: MappingProcessor = new MappingProcessor();

const mappingPromise = mappingProcessor.processMappingFile('mapping.csv');

mappingPromise.then((mapping: Record<string, string>) => {
  return processor.process('data.csv', myId, mapping);
}).then((result: ITimeRecordOptions[]) => {
  console.log('CSV file successfully processed');
  console.log(result);

  if(actuallyRun) {
    result.forEach((entry: ITimeRecordOptions) => {
      freedcampApi.addTimeRecord(entry).then((response) => {
        console.log(JSON.stringify(response.data));
      })
      .catch((error) => {
        console.log(error);
      });
    })
  }
  return result;
});

