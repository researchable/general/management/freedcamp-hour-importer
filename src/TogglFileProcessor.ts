import csv from 'csv-parser';
import fs from 'fs';
import { convertTimeToSeconds } from './Util';
import { ITimeRecordOptions } from './freedcamp_api/use_cases/AddTimeRecord';
import { IFileProcessor } from './IFileProcessor';

export interface IToggleEntry {
  User: string;
  Email: string;
  Client: string;
  Project: string;
  Task: string;
  Description: string;
  Billable: string;
  'Start date': string;
  'Start time': string;
  'End date': string;
  'End time': string;
  Duration: string;
  Tags: string;
  Amount: string;
}

export class ToggleFileProcessor implements IFileProcessor {
  private processTogglRow = (row: IToggleEntry, myId: string, mapping: Record<string, string>): ITimeRecordOptions => {
    if(row.Client === undefined) {
      throw(new Error('The CLIENT is not found in the data. Make sure you convert the data'));
    }
    return this.convertTogglToFreedcamp(row, myId, mapping);
  }

  private processDescription = (description: string, projectId: string) => {
    const regex = /\[[0-9, ]*\]/g;
    const found = description.match(regex);

    if (found === null) return description;

    const finalDesc = found.reduce((acc, foundStr) => {
      const taskIds = foundStr
        .replace('[', '')
        .replace(']', '')
        .split(',')
        .map((x) => x.trim());

      const newDesc = taskIds.reduce((acc, taskId) => {
        return `${acc}https://freedcamp.com/view/${projectId}/tasks/${taskId} `;
      }, '')
      return `${acc} ${newDesc}`;
    }, '')
    return finalDesc + description;
  }

  private convertTogglToFreedcamp = (togglEntry: IToggleEntry, myId: string, mapping: Record<string, string>): ITimeRecordOptions => { 
    const seconds = convertTimeToSeconds(togglEntry.Duration);
    const minutes = (seconds / 60);
    const projectId = mapping[togglEntry.Project];
    if(projectId === undefined) {
      throw(new Error(`The ID for the project ${togglEntry.Project} was not found (${togglEntry.Client})`));
    }

    const description = this.processDescription(togglEntry.Description, projectId);
    const result = {
      description: description,
      project_id: projectId,
      assigned_to_id: myId,
      date: togglEntry['Start date'],
      minutes_count: `${minutes}`,
      f_started: 1, // Set to completed
      f_billed: ''
    };
    return result;
  }

  public process = (fileName: string, myId: string, mapping: Record<string, string>): Promise<ITimeRecordOptions[]> => {
    const records: ITimeRecordOptions[] = [];
    return new Promise((resolve, reject) => {
      fs.createReadStream(fileName)
        .pipe(csv())
        .on('error', reject)
        .on('data', (row: IToggleEntry) => {
          records.push(this.processTogglRow(row, myId, mapping));
        })
        .on('end', () => {
          resolve(records);
        });
    });
  }
}

