import axios, { AxiosRequestConfig } from 'axios';
import formData from 'form-data';

export interface ITimeRecordOptions {
  description: string;
  project_id: string;
  assigned_to_id: string;
  date: string;
  minutes_count: string;
  f_started: number;
  f_billed: string;
}

export default function execute(apiKey: string, baseUrl: string): unknown {
  return (timeRecordOptions: ITimeRecordOptions): Promise<string> => {
    const data = new formData();
    data.append('data', JSON.stringify(timeRecordOptions));

    const config: AxiosRequestConfig = {
      data,
      method: 'post',
      url: `${baseUrl}/times?api_key=${apiKey}`,
      headers: data.getHeaders()
    };

    return new Promise((resolve, reject): void => {
      axios(config).then((result) => {
        resolve(JSON.stringify(result.data))
      }).catch((err) => {
        reject(err)
      })
    })
  }
}
