import { ITimeRecordOptions } from './freedcamp_api/use_cases/AddTimeRecord';

export interface IFileProcessor {
  process(fileName: string, myId: string, mapping: Record<string, string>): Promise<ITimeRecordOptions[]>;
}
