# Freedcamp Hour Importer

## Creating a mapping
Creact a file called `mapping.csv` and add the following attributes in there:

```
freedcamp_project,id,toggl_reference
"Sport Data Valley",2354427,"Sport Data Valley"
"General",2593377,"General"
"General",2593377,"VentureLab"
"General",2593377,"Administration"
"General",2593377,"Grant proposal"
"[MIT] Explainable AI",2768879,"MIT: Explainable AI"
"ISO27001 / NEN 7510",2822669,"ISO27001 / NEN7510"
"iShared",2354428,"Samen Sturen"
```
